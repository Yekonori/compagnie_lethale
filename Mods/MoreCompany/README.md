### [More Company](https://thunderstore.io/c/lethal-company/p/notnotnotswipez/MoreCompany/)

> A stable lobby player count expansion mod. With cosmetics!



> Yes, although there is another mod that does a very similar thing, it has a few stability issues regarding the functionality of the game. In the development of MoreCompany, I had stability and polish in mind, I wanted to make a relatively frictionless experience regarding expanded player counts.

> In my testing of an 8 player lobby, I experienced no issues nor did anyone else in the lobby. Alot of game conditions were tested, but there may be conditions which were not tested that may be broken or cause a softlock. If you experience issues like these, please let me know at the Discord server linked at the bottom of this page.

> This mod is not affiliated with BiggerLobby/LethalPlayers nor does it use any code from that mod! Any similarities are purely coincidental.

---



#### Installation: 

A recommended mod for peoples who want to play some chaotic runs... after all, you can play with 31 friends with it!

The installation is quite simple, all you have to do is to copy and paste the `MoreCompany.dll` file in `BepInEx/plugins`... and yes, if they're no plugins folder in `BepInEx` you need to create it.



If you have no other mods installed, you plugins folder look like that: 

![MoreCompanyInstalled](../../Images/MoreCompanyInstalled.png)





---

**Current version:** 1.7.4 (compatible with *Lethal Company v47*)

**Last update:** 12/01/2024