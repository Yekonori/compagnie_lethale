### [BepInEx](https://thunderstore.io/c/lethal-company/p/BepInEx/BepInExPack/)

> BepInEx pack for Mono Unity games. Preconfigured and ready to use.



> BepInEx is a general purpose framework for Unity modding. BepInEx includes tools and libraries to
>
> - load custom code (hereafter *plugins*) into the game on launch;
> - patch in-game methods, classes and even entire assemblies without touching original game files;
> - configure plugins and log game to desired outputs like console or file;
> - manage plugin dependencies.
>
> BepInEx is currently [one of the most popular modding tools for Unity on GitHub](https://github.com/topics/modding?o=desc&s=stars).

---



#### Installation: 

Well, it's not that hard, all you have to do is to copy then past the `BepInEx` folder, `doorstop_config.ini` and `winhttp.dll` files to the root of your game.
The root of the game can easily be reached with Steam : 

![AccessToGameRoot](../../Images/AccessToGameRoot.png)



So at the end your game folder need to be like that:

![BepInExInstalled](../../Images/BepInExInstalled.png)



---

**Current version:** 5.4.21

**Last update:** 12/01/2024